package com.example.TestTask.exception;

public class RegisterException extends RuntimeException {
    public RegisterException(String errorMessage) {
        super(errorMessage);
    }
}
