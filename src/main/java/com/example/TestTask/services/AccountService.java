package com.example.TestTask.services;

import com.example.TestTask.api.request.RegisterRq;
import com.example.TestTask.api.response.RegisterRs;
import com.example.TestTask.exception.RegisterException;
import com.example.TestTask.model.Person;
import com.example.TestTask.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final PersonRepository personRepository;

    public RegisterRs getRegisterData(RegisterRq regRequest) {

        validateFields(regRequest);
        personRepository.save(getPerson(regRequest));

        RegisterRs registerRs = new RegisterRs();
        registerRs.setMessage("registration is completed");
        registerRs.setEmail(regRequest.getEmail());

        return registerRs;
    }

    private Person getPerson(RegisterRq regRequest) {
        Person person = new Person();
        person.setEmail(regRequest.getEmail());
        person.setUsername(regRequest.getUsername());
        person.setName(regRequest.getName());

        return person;
    }

    @SneakyThrows
    private void validateFields(RegisterRq regRequest) {
        if (personRepository.findByEmail(regRequest.getEmail()) != null) {
            throw new RegisterException("a user with such an email already exists");
        }

        if (!EmailValidator.getInstance().isValid(regRequest.getEmail())) {
            throw new RegisterException("incorrect email");
        }

        if (personRepository.findByUsername(regRequest.getUsername()) != null) {
            throw new RegisterException("a user with such an username already exists");
        }

        if (!regRequest.getUsername().matches("\\w+")) {
            throw new RegisterException("invalid characters");
        }
    }
}
