package com.example.TestTask.services;

import com.example.TestTask.api.response.MoviesPageRs;
import com.example.TestTask.api.response.MoviesRs;
import com.example.TestTask.exception.RegisterException;
import com.example.TestTask.model.Movie;
import com.example.TestTask.repository.FavoriteMovieRepository;
import com.example.TestTask.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Validated
@EnableScheduling
public class MovieService {

    @Value("${search.themoviedbToken}")
    private String themoviedbToken;

    private final MovieRepository movieRepository;
    private final FavoriteMovieRepository favoriteMovieRepository;

    @Scheduled(fixedDelay = 10_800_000)
    public void moviesRider() throws IOException {

        movieRepository.clean();

        for (int i = 1; i <= 5; i++) {
            JSONArray movieList = MoviesSearch(i);

            for (int j = 0; j < movieList.length(); j++) {
                String title = movieList.getJSONObject(j).getString("original_title");
                String posterPath = movieList.getJSONObject(j).getString("poster_path");

                Movie movie = new Movie();
                movie.setTitle(title);
                movie.setPosterPath(posterPath);

                movieRepository.save(movie);
            }
        }
    }

    public JSONArray MoviesSearch(Integer page) throws IOException {

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://api.themoviedb.org/3/discover/movie?page=" + page)
                .get()
                .addHeader("accept", "application/json")
                .addHeader("Authorization", themoviedbToken)
                .build();

        Response response = client.newCall(request).execute();

        JSONObject obj = new JSONObject(response.body().string());
        JSONArray movieList = obj.getJSONArray("results");

        return movieList;
    }

    public MoviesPageRs moviesInDatabase(Integer page) {

        return pagesMoviesSort(movieRepository.getMovies(),page);
    }

    public MoviesRs addFavorites(String favorite) {

        validateAddFavorite(favorite);

        Movie movie = new Movie();
        movie.setTitle(movieRepository.findByTitle(favorite).getTitle());
        movie.setPosterPath(movieRepository.findByTitle(favorite).getPosterPath());

        favoriteMovieRepository.addFavorite(movie);

        MoviesRs addFavoriteRs = new MoviesRs();
        addFavoriteRs.setMessage("movie added to favorites");
        addFavoriteRs.setTitle(movie.getTitle());
        addFavoriteRs.setPosterPath(movie.getPosterPath());

        return addFavoriteRs;
    }

    public MoviesRs removeFavorites(String favorites) {

        validateRemoveFavorite(favorites);

        Movie movie = new Movie();
        movie.setTitle(favoriteMovieRepository.findByTitle(favorites).getTitle());
        movie.setPosterPath(favoriteMovieRepository.findByTitle(favorites).getPosterPath());

        favoriteMovieRepository.FavoriteRemove(movie.getTitle());

        MoviesRs favoriteRs = new MoviesRs();
        favoriteRs.setMessage("Movie removed from favorites");
        favoriteRs.setTitle(movie.getTitle());
        favoriteRs.setPosterPath(movie.getPosterPath());

        return favoriteRs;
    }

    public MoviesPageRs notFavorites(String query, Integer page) {

        MoviesPageRs moviesPageRs = new MoviesPageRs();

        if (query.equals("inMemory")) {

            moviesPageRs = pagesMoviesSort(inMemory(), page);

            validateRemovePage(page,inMemory());
        }

        if (query.equals("sql")) {

            List<MoviesRs> sql = movieRepository.getNotFavorites();
            moviesPageRs = pagesMoviesSort(sql, page);

            validateRemovePage(page,sql);
        }


        return moviesPageRs;
    }

    public MoviesPageRs pagesMoviesSort (List<MoviesRs> list, Integer page) {

        int pageIndex1 = 15 * page;
        int pageIndex2 = 15;

        validateRemovePage(page, list);

        List<MoviesRs> pageSearch = new ArrayList<>();
        MoviesPageRs moviesPageRs = new MoviesPageRs();


        if (pageIndex1 >= list.size()) {
            pageIndex1 = list.size();
            pageIndex2 = pageIndex1 - (page - 1) * 15;
        } else if (list.size() < 15) {
            pageIndex1 = list.size();
            pageIndex2 = pageIndex1;
        }

        for (int i = pageIndex1 - pageIndex2; i < pageIndex1; i++) {
            pageSearch.add(list.get(i));
        }

        moviesPageRs.setPage(page);
        moviesPageRs.setMoviesList(pageSearch);

        return moviesPageRs;
    }

    public List<MoviesRs> inMemory() {
        List<MoviesRs> allMovies = movieRepository.getMovies();
        List<MoviesRs> favoritesMovies = favoriteMovieRepository.getMovies();

        for (int i = 0; i < favoritesMovies.size(); i++) {
            MoviesRs moviesRs = favoritesMovies.get(i);
            allMovies.remove(moviesRs);
        }

        return allMovies;
    }

    @SneakyThrows
    private void validateAddFavorite(String title) {
        if (favoriteMovieRepository.findByTitle(title) != null) {
            throw new RegisterException("this movie is already in favorites");
        }

        if (movieRepository.findByTitle(title) == null) {
            throw new RegisterException("there is no movie with this name");
        }
    }

    @SneakyThrows
    private void validateRemoveFavorite(String title) {

        if (favoriteMovieRepository.findByTitle(title) == null) {
            throw new RegisterException("This movie is not in favorites");
        }
    }

    @SneakyThrows
    private void validateRemovePage(Integer page, List<MoviesRs> moviesRs) {

        if (page > (moviesRs.size()/15) + 1) {
            throw new RegisterException("Page not found");
        }
    }
}
