package com.example.TestTask.services;

import com.example.TestTask.api.request.UserRq;
import com.example.TestTask.api.response.PersonRs;
import com.example.TestTask.exception.RegisterException;
import com.example.TestTask.model.Person;
import com.example.TestTask.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@RequiredArgsConstructor
@Validated
public class UserService {

    private final PersonRepository personRepository;

    public PersonRs getUserById(String email) {

        validateUser(email);

        Person person = personRepository.findByEmail(email);

        PersonRs personRs = new PersonRs();
        personRs.setMessage("User Information");
        personRs.setEmail(person.getEmail());
        personRs.setUsername(person.getUsername());
        personRs.setName(person.getName());

        return personRs;
    }

    public PersonRs updateUserInfo(String email, UserRq userRq) {

        validateUser(email);

        if (userRq.getName() != null) {
            personRepository.updatePersonName(userRq.getName(), email);
        }

        if (userRq.getUsername() != null) {
            validateFieldsUpdate(userRq);
            personRepository.updatePersonUsername(userRq.getUsername(), email);
        }

        Person person = personRepository.findByEmail(email);

        PersonRs personRs = new PersonRs();
        personRs.setMessage("User data changed");
        personRs.setEmail(person.getEmail());
        personRs.setUsername(person.getUsername());
        personRs.setName(person.getName());

        return personRs;
    }

    public PersonRs delete(String email) {

        validateFieldsEmail(email);

        PersonRs personRs = new PersonRs();
        personRs.setMessage("user deleted");
        personRs.setEmail(email);

        personRepository.UserDelete(email);

        return personRs;
    }

    @SneakyThrows
    private void validateFieldsUpdate(UserRq userRq) {

        if (!userRq.getUsername().matches("\\w+")) {
            throw new RegisterException("invalid characters");
        }
    }

    @SneakyThrows
    private void validateUser(String email) {

        if (personRepository.findByEmail(email) == null) {
            throw new RegisterException("the user does not exist");
        }
    }

    @SneakyThrows
    private void validateFieldsEmail(String email) {

        if (personRepository.findByEmail(email) == null) {
            throw new RegisterException("the user does not exist");
        }
    }
}