package com.example.TestTask.repository;

import com.example.TestTask.model.Person;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class PersonRepository {

    private final JdbcTemplate jdbcTemplate;

    private static final String SELECT = "SELECT * FROM persons";
    public static final RowMapper<Person> PERSON_ROW_MAPPER = (resultSet, rowNum) -> {
        Person person = new Person();
        person.setId(resultSet.getLong("id"));
        person.setEmail(resultSet.getString("email"));
        person.setUsername(resultSet.getString("username"));
        person.setName(resultSet.getString("name"));

        return person;
    };

    public void save(Person person) {
        jdbcTemplate.update(
                "INSERT INTO persons " +
                        "(email, username, name) " +
                        "VALUES (?, ?, ?)",
                person.getEmail().toLowerCase(),
                person.getUsername(),
                person.getName()
        );
    }

    public Person findByEmail(String email) {
        try {
            return jdbcTemplate.queryForObject(
                    SELECT + " WHERE lower(email) = ?",
                    PERSON_ROW_MAPPER,
                    email.toLowerCase()
            );
        } catch (EmptyResultDataAccessException ignored) {
            return null;
        }
    }

    public Person findByUsername(String username) {
        try {
            return jdbcTemplate.queryForObject(
                    SELECT + " WHERE lower(username) = ?",
                    PERSON_ROW_MAPPER,
                    username.toLowerCase()
            );
        } catch (EmptyResultDataAccessException ignored) {
            return null;
        }
    }

    public void updatePersonUsername(String username, String email) {

        jdbcTemplate.update("Update Persons Set username = ? Where email = ?",
                username,
                email);
    }

    public void updatePersonName(String name, String email) {

        jdbcTemplate.update("Update Persons Set name = ? Where email = ?",
                name,
                email);
    }

    public void UserDelete(String email) {
        jdbcTemplate.update("Delete from Persons Where email = ?", email);
    }
}