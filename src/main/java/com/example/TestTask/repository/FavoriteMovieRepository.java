package com.example.TestTask.repository;

import com.example.TestTask.api.response.MoviesRs;
import com.example.TestTask.model.Movie;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class FavoriteMovieRepository {

    private final JdbcTemplate jdbcTemplate;

    private static final String SELECT = "SELECT * FROM movies_favorites";

    public void addFavorite(Movie movie) {
        jdbcTemplate.update(
                "INSERT INTO movies_favorites " +
                        "(title, poster_path) " +
                        "VALUES (?, ?)",
                movie.getTitle(),
                movie.getPosterPath()
        );
    }

    public MoviesRs findByTitle(String username) {
        try {
            return jdbcTemplate.queryForObject(
                    SELECT + " WHERE lower(title) = ?",
                    MOVIE_FAVORITE_ROW_MAPPER,
                    username.toLowerCase()
            );
        } catch (EmptyResultDataAccessException ignored) {
            return null;
        }
    }

    public void FavoriteRemove(String title) {
        jdbcTemplate.update("Delete from Movies_favorites Where title = ?", title);
    }

    public List<MoviesRs> getMovies() {
        try {
            return jdbcTemplate.query("SELECT DISTINCT movies_favorites.id, movies_favorites.title, " +
                    "movies_favorites.poster_path FROM movies_favorites ORDER BY id ASC", MOVIE_FAVORITE_ROW_MAPPER);
        } catch (EmptyResultDataAccessException ignored) {
            return Collections.emptyList();
        }
    }

    private final RowMapper<MoviesRs> MOVIE_FAVORITE_ROW_MAPPER = (resultSet, rowNum) -> {
        MoviesRs moviesRs = new MoviesRs();
        moviesRs.setTitle(resultSet.getString(2));
        moviesRs.setPosterPath(resultSet.getString(3));
        return moviesRs;
    };
}
