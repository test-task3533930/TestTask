package com.example.TestTask.repository;

import com.example.TestTask.api.response.MoviesRs;
import com.example.TestTask.model.Movie;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class MovieRepository {

    private final JdbcTemplate jdbcTemplate;

    private static final String SELECT = "SELECT * FROM movies";

    public MoviesRs findByTitle(String username) {
        try {
            return jdbcTemplate.queryForObject(
                    SELECT + " WHERE lower(title) = ?",
                    MOVIE_ROW_MAPPER,
                    username.toLowerCase()
            );
        } catch (EmptyResultDataAccessException ignored) {
            return null;
        }
    }

    public void save(Movie movie) {
        jdbcTemplate.update(
                "INSERT INTO movies " +
                        "(title, poster_path) " +
                        "VALUES (?, ?)",
                movie.getTitle(),
                movie.getPosterPath()
        );
    }

    public void clean() {
        jdbcTemplate.update(
                "TRUNCATE TABLE movies"
        );
    }

    public List<MoviesRs> getMovies() {
        try {
            return jdbcTemplate.query("SELECT DISTINCT movies.id, movies.title, movies.poster_path FROM movies ORDER BY id ASC", MOVIE_ROW_MAPPER);
        } catch (EmptyResultDataAccessException ignored) {
            return Collections.emptyList();
        }
    }

    public List<MoviesRs> getNotFavorites() {
        try {
            return jdbcTemplate.query("SELECT * FROM movies WHERE title NOT IN (SELECT title FROM movies_favorites);", MOVIE_ROW_MAPPER);
        } catch (EmptyResultDataAccessException ignored) {
            return Collections.emptyList();
        }
    }

    private final RowMapper<MoviesRs> MOVIE_ROW_MAPPER = (resultSet, rowNum) -> {
        MoviesRs moviesRs = new MoviesRs();
        moviesRs.setTitle(resultSet.getString(2));
        moviesRs.setPosterPath(resultSet.getString(3));
        return moviesRs;
    };
}
