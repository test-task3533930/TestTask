package com.example.TestTask.api.response;

import lombok.Data;

import java.util.List;

@Data
public class ListMoviesRs {
    private List<MoviesRs> list;

    public ListMoviesRs(List<MoviesRs> list) {
        this.list = list;
    }
}
