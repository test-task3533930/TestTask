package com.example.TestTask.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonRs {

    private String message;

    private String email;

    private String username;

    private String name;
}
