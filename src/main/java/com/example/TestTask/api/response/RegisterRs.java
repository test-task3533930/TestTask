package com.example.TestTask.api.response;

import lombok.Data;

@Data
public class RegisterRs {

    private String message;

    private String email;

}
