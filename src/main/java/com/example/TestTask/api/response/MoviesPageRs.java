package com.example.TestTask.api.response;

import lombok.Data;

import java.util.List;

@Data
public class MoviesPageRs {

    private Integer page;

    private List<MoviesRs> moviesList;
}
