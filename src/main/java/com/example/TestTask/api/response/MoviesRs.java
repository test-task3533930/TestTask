package com.example.TestTask.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MoviesRs {

    private String message;

    private String title;

    private String posterPath;
}
