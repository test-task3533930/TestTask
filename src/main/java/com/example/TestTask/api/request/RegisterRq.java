package com.example.TestTask.api.request;

import lombok.Data;

@Data
public class RegisterRq {

    private String email;

    private String username;

    private String name;
}