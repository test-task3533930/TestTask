package com.example.TestTask.api.request;

import lombok.Data;

@Data
public class UserRq {

    private String username;

    private String name;

}
