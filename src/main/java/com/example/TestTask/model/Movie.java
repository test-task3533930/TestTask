package com.example.TestTask.model;

import lombok.Data;

@Data
public class Movie {

    private Long id;

    private String title;

    private String posterPath;
}
