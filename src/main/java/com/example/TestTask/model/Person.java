package com.example.TestTask.model;

import lombok.Data;

@Data
public class Person {

    private Long id;

    private String email;

    private String username;

    private String name;
}