package com.example.TestTask.controllers;

import com.example.TestTask.api.request.RegisterRq;
import com.example.TestTask.api.response.RegisterRs;
import com.example.TestTask.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping("/register")
    public RegisterRs register(@Valid @RequestBody @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "regRequest") RegisterRq regRequest) {
        return accountService.getRegisterData(regRequest);
    }
}