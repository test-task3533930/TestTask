package com.example.TestTask.controllers;

import com.example.TestTask.api.request.UserRq;
import com.example.TestTask.api.response.PersonRs;
import com.example.TestTask.services.UserService;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService personService;
    @GetMapping("/me")
    public PersonRs getUserById(@RequestHeader(name = "user")String email) {
        return personService.getUserById(email);
    }

    @PutMapping("/me")
    public PersonRs updateUserInfo(@RequestHeader(name = "user")String email,
                                   @RequestBody @io.swagger.v3.oas.annotations.parameters.RequestBody
            (description = "userData")UserRq userRq) {
        return personService.updateUserInfo(email,userRq);
    }

    @DeleteMapping("/me")
    public PersonRs deleteUser(@RequestHeader(name = "user")String email) {
        return personService.delete(email);
    }
}
