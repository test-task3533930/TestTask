package com.example.TestTask.controllers;

import com.example.TestTask.api.response.MoviesPageRs;
import com.example.TestTask.api.response.MoviesRs;
import com.example.TestTask.services.MovieService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/movies")
@RequiredArgsConstructor
public class MoviesController {

    private final MovieService movieService;

    @GetMapping("/list")
    public MoviesPageRs moviesRs(@RequestParam(name = "page", defaultValue = "1") Integer page) {
        return movieService.moviesInDatabase(page);
    }

    @GetMapping("/favorites")
    public MoviesRs addFavoriteRs(@RequestParam(name = "add") String favorite) {
        return movieService.addFavorites(favorite);
    }

    @DeleteMapping("/favorites")
    public MoviesRs removeFavoriteRs(@RequestParam(name = "remove") String favorite) {
        return movieService.removeFavorites(favorite);
    }

    @GetMapping("/notFavoriteInMemory")
    public MoviesPageRs findSql(@RequestParam(name = "loaderType") String query,
                                @RequestParam(name = "page", defaultValue = "1") Integer page) {
        return movieService.notFavorites(query,page);
    }
}
